This is a group project that I worked on for a game programming class.

The game is made in unity using music by stellardrone, and free art from solcommand.com


The build of the game is in the Final Build folder and the source code is in the Unity Project folder.

### How do I get set up? ###


A build of the game is included in the repo, so hopefull just download the folder and launch the executable.


As for the source code, it has not been adapted to unity 5.0, so its incompatible with current versions at the moment.  

### Contribution guidelines ###

Created by Connor Jensen, Richard Luu, and Jeff Bunce

Repo was re-uploaded from gitgud.io